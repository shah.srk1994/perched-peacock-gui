import {VehicleType} from '../../../shared/models/parking-lot';

export class ParkingLog {
  id: number;
  parkingLotId: number;
  vehicleType: VehicleType;
  vehicleNumber: string;
  vehicleWeight: number;
  enterTimeEpoch: number;
  exitTimeEpoch: number;
  charge: number;
  currency: string;
}

export class ParkingLogRequest {
  parkingLotId: number;
  vehicleType: VehicleType;
  vehicleNumber: string;
  vehicleWeight: number;
}
