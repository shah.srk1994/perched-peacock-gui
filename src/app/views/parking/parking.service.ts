import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BASE_URL} from '../../app.component';
import {AuthenticationService} from '../../shared/services/authentication.service';

@Injectable()
export class ParkingService {

  constructor(private http: HttpClient,
              private authenticationService: AuthenticationService) {
  }

  fetchParkingLogs(params): Observable<any> {
    const uri = BASE_URL + '/api/v1/parking-log';
    const headers = this.getHttpHeaders();
    return this.http.get<any>(uri, {params, headers});
  }

  // Fetch parking logs for all parked vehicles for a particular parking lot
  fetchParkedVehicleLogs(parkingLotId, filter: string = ''): Observable<any> {
    const params = {
      filter: `exitTimeEpoch<0%parkingLotId^${parkingLotId}%${filter}`,
    };
    const headers = this.getHttpHeaders();
    const uri = BASE_URL + '/api/v1/parking-log';
    return this.http.get<any>(uri, {params, headers});
  }

  parkVehicle(request): Observable<any> {
    const uri = BASE_URL + '/api/v1/parking-log/parkVehicle';
    const headers = this.getHttpHeaders();
    return this.http.post<any>(uri, request, {headers});
  }

  exitVehicle(request): Observable<any> {
    const uri = BASE_URL + '/api/v1/parking-log/exitVehicle';
    const headers = this.getHttpHeaders();
    return this.http.post<any>(uri, request, {headers});
  }

  private getHttpHeaders() {
    return new HttpHeaders({
      'Authorization': `Basic ${this.authenticationService.currentUserValue.authData}`
    });
  }
}
