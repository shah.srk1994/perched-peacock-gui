# Welcome to Perched Peacock GUI Project

[http://perched-peacock-gui.ap-south-1.elasticbeanstalk.com/](http://perched-peacock-gui.ap-south-1.elasticbeanstalk.com/)

![Pipeline status](https://gitlab.com/shah.srk1994/perched-peacock-gui/badges/master/pipeline.svg)

Car parking is a major problem in urban areas in both developed and developing countries. Following the rapid incense of car ownership, many cities are suffering from lacking of car parking areas with imbalance between parking supply and demand which can be considered the initial reason for metropolis parking problems.
Perched Peacock aims to solve this problem by providing a real-time parking slot availibility information to its customers and let them book their parking spot in advance*.

The Home Page consists of 2 tabs

 1. Home - This is where customers can search for a parking lot and check if there is any available spot.
 2. My Parking Lot - This is where the agents can see the logs of vehicle currently parked in the lot.Agents will have to login with parking lot ID and password to access this tab. Agents will also have ability to make a entry/exit log.

Based on the entry/exit log, the home page will provide customers real-time parking lot availibity on their finger tips.

For Testing, Login using the following credentials:
*  Id = 1
*  Password = password

## The FrontEnd
The backend service of perched peacock is build using [Angular](https://angular.io/) Framework.

## Getting Started

### Requirements

 1. npm
 2. angular cli

To make sure its installed correctly, execute these command

	npm -v
	ng v

These should print the current installed version respectively.

### Running it locally

To run the application locally, you need to build a jar using maven and then run it.

	git clone https://gitlab.com/shah.srk1994/perched-peacock-gui.git
	cd perched-peacock-gui
	ng serve -c local
	
You can then access the GUI using http://localhost:4200

##  Pipeline

The project pipeline is mainly divided into 3 stages:

#### 1. Build
   *  There are 2 jobs in this stage:
   * `build for non prod` - Source code is compilied and package is created using non prod variables
   * `build for prod` - Source code is compilied and package is created using prod variables
   * `test` - Unit and integration tests run to make sure no tests are breaking
#### 2. Deploy
   * There are 2 jobs in this stage:
   * `deploy to non prod` -  deploys the code to dev environment. This job triggers only when a change is commited to the **develop** branch 
   * `deploy to prod` - deploys the code to prod environment. This job triggers only when a change is commited to the **master** branch 


Checkout a pipeline [here](https://gitlab.com/shah.srk1994/perched-peacock-gui/pipelines/88692716).

![Pipeline](images/Capture.JPG)

## Branching Stategy
For Branching stategy, we follow [Git Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows)

## Contribution
To contribute, please follow these simple workflow.

1. Make sure your changes are up to date with the deploy branch, If not please rebase the feature branch on develop.
2. Make sure the branch name follows convention of either `feature/newFeature` if it's a feature or `hotfix/fixPerformed` incase of hotfixes. 
3. Once your changes are merged, you can access them here [**non-prod**](http://perched-peacock-gui-dev.ap-south-1.elasticbeanstalk.com/) and [**prod**](http://perched-peacock-gui.ap-south-1.elasticbeanstalk.com/)
