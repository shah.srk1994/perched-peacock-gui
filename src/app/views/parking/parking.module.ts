import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {ParkingComponent} from './parking.component';
import {ParkingService} from './parking.service';
import {ParkingRoutingModule} from './parking-routing.module';
import {ModalModule} from 'ngx-bootstrap';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    ParkingRoutingModule,
    ModalModule.forRoot(),
    HttpClientModule
  ],
  declarations: [ParkingComponent],
  providers: [ParkingService]
})

export class ParkingModule {
}
