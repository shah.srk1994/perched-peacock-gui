import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../shared/services/authentication.service';
import {Router} from '@angular/router';
import {User} from '../../shared/models/user';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

  private user: User;

  constructor(private authenticationService: AuthenticationService,
              private router: Router) {
  }

  ngOnInit() {
  }

  onSubmitClick(userId, password) {
    this.authenticationService.login(userId, password)
      .subscribe(response => {
        this.router.navigate(['/home']);
      });
  }

}
