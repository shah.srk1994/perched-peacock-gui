import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login.component';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    HttpClientModule
  ],
  declarations: [LoginComponent],
  providers: []
})

export class LoginModule {
}
