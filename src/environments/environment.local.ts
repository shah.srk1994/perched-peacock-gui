// For Local Testing. Server needs to be running in local.
export const environment = {
  production: false,
  backendBaseUrl: 'http://localhost:5000'
};
