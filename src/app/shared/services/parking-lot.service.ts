import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BASE_URL} from '../../app.component';

@Injectable({providedIn: 'root'})
export class ParkingLotService {

  constructor(private http: HttpClient) {
  }

  fetchParkingLots(params): Observable<any> {
    const uri = BASE_URL + '/api/v1/parking-lot';
    return this.http.get<any>(uri, {params});
  }

  fetchParkingLotById(id: number): Observable<any> {
    const uri = BASE_URL + `/api/v1/parking-lot/${id}`;
    return this.http.get<any>(uri);
  }

}
