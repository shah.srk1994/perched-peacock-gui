import {Component, OnInit, ViewChild} from '@angular/core';
import {ParkingLog, ParkingLogRequest} from './models/parking-log';
import {ParkingService} from './parking.service';
import {ModalDirective} from 'ngx-bootstrap';
import {AuthenticationService} from '../../shared/services/authentication.service';
import {ParkingLot, VehicleType} from '../../shared/models/parking-lot';
import {AlertService} from '../../shared/alert/alert.service';
import {ParkingLotService} from '../../shared/services/parking-lot.service';

@Component({
  selector: 'app-parking',
  templateUrl: './parking.component.html',
})

export class ParkingComponent implements OnInit {

  @ViewChild('parkVehicleModal', {static: false})
  public parkVehicleModal: ModalDirective;

  public vehicleType = VehicleType;

  public parkedVehicles: ParkingLog[];
  public parkingLogRequest: ParkingLogRequest = new ParkingLogRequest();
  private parkingLotId: number;

  public parkingLot: ParkingLot;

  constructor(private parkingService: ParkingService,
              private authenticationService: AuthenticationService,
              private alertService: AlertService,
              private parkingLotService: ParkingLotService) {
  }

  currentEpoch(): number {
    return Date.now();
  }

  ngOnInit(): void {
    this.parkingLotId = this.authenticationService.currentUserValue.userId;
    this.fetchParkedVehicles();
    this.fetchParkingLot();
  }

  fetchParkedVehicles(filter: string = '') {
    this.parkingService.fetchParkedVehicleLogs(this.parkingLotId, filter)
      .subscribe(response => {
        this.parkedVehicles = response.dataList;
      });
  }

  fetchParkingLot() {
    this.parkingLotService.fetchParkingLotById(this.parkingLotId)
      .subscribe(parkingLot => {
        this.parkingLot = parkingLot;
        parkingLot.vehicleTypeInfoSet.forEach(vehicleTypeInfo => {
          this.parkingLot[vehicleTypeInfo.vehicleInfoPK.vehicleType] = vehicleTypeInfo;
        });
      });
  }

  exitVehicle(parkedVehicle: ParkingLog) {
    this.createParkingLogRequestFromParkedVehicle(parkedVehicle);
    this.parkingService.exitVehicle(this.parkingLogRequest)
      .subscribe(response => {
        this.alertService.success(response.response);
        this.fetchParkedVehicles();
        this.fetchParkingLot();
      });
  }

  onFormSubmit() {
    this.parkingLogRequest.parkingLotId = this.parkingLotId;
    this.parkingService.parkVehicle(this.parkingLogRequest)
      .subscribe(response => {
        this.alertService.success(response.response);
        this.fetchParkedVehicles();
        this.fetchParkingLot();
      });
  }

  onSearchButtonClick(searchValue) {
    const filter = `vehicleNumber~${searchValue}`;
    this.fetchParkedVehicles(filter);
  }

  createParkingLogRequestFromParkedVehicle(parkedVehicle: ParkingLog) {
    this.parkingLogRequest.parkingLotId = parkedVehicle.parkingLotId;
    this.parkingLogRequest.vehicleType = parkedVehicle.vehicleType;
    this.parkingLogRequest.vehicleNumber = parkedVehicle.vehicleNumber;
    this.parkingLogRequest.vehicleWeight = parkedVehicle.vehicleWeight;
  }

}
