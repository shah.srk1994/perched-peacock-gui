import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {BASE_URL} from '../../app.component';
import {User} from '../models/user';

@Injectable({providedIn: 'root'})
export class AuthenticationService {

  public currentUser: Observable<User>;
  private currentUserSubject: BehaviorSubject<User>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(userId: string, password: string) {
    const uri = BASE_URL + '/api/v1/login';
    const user = new User();
    const headers = this.getHttpOptions(userId, password);
    return this.http.post<any>(uri, {userId}, headers)
      .pipe(map(response => {
        user.userId = +userId;
        user.authData = window.btoa(userId + ':' + password);
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        return user;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

  private getHttpOptions(userId, password) {
    const authToken = window.btoa(userId + ':' + password);
    return {
      headers: new HttpHeaders({
        'Authorization': `Basic ${authToken}`
      })
    };
  }
}
