import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HomeComponent} from './home.component';
import {HomeRoutingModule} from './home-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {ChartsModule} from 'ng2-charts';
import {MatGridListModule} from '@angular/material';

@NgModule({
  imports: [
    FormsModule,
    ChartsModule,
    HomeRoutingModule,
    CommonModule,
    HttpClientModule,
    MatGridListModule
  ],
  declarations: [HomeComponent],
  providers: []
})

export class HomeModule {
}
