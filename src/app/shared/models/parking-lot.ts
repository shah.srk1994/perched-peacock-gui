export class ParkingLot {
  private id: number;
  private parkingLotName: string;
  private address: string;
  private city: string;
  private state: string;
  private country: string;
  private vehicleTypeInfoSet: VehicleTypeInfo[];
}

export class VehicleTypeInfo {
  private vehicleInfoPK: { parkingLot: number, vehicleType: VehicleType };
  private chargePerHour: number;
  private currency: string;
  private maxOccupancy: number;
  private vacancy: number;
}

export enum VehicleType {
  TWO_WHEELER = 'TWO_WHEELER',
  FOUR_WHEELER = 'FOUR_WHEELER'
}
