import {Component, OnInit} from '@angular/core';
import {ParkingLotService} from '../../shared/services/parking-lot.service';
import {Router} from '@angular/router';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  public parkingLots = [];

  public filter = '';

  constructor(private parkingLotService: ParkingLotService,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  onFindButtonClick(searchValue) {
    this.parkingLots = [];
    const params = this.getRequestParams(searchValue.value);

    this.parkingLotService.fetchParkingLots(params)
      .subscribe(response => {
        const parkingLotList = response.dataList;
        parkingLotList.forEach(parkingLot => {
          const parkingLotInfo = parkingLot;
          parkingLot.vehicleTypeInfoSet.forEach(vehicleTypeInfo => {
            parkingLotInfo[vehicleTypeInfo.vehicleInfoPK.vehicleType] = vehicleTypeInfo;
          });
          this.parkingLots.push(parkingLotInfo);
        });
      });

  }

  getRequestParams(searchValue) {
    if (searchValue.value !== null || searchValue.value !== undefined) {
      this.filter = 'parkingLotName~' + searchValue + '#address~' + searchValue + '#city~' + searchValue;
    }
    return {filter: this.filter};
  }

}
